package com.rostyslavprotsiv.view;

public final class Menu extends AbstractMenu {
    private int clientId;
    private String clientName;
    private String clientSurname;
    private String clientMiddleName;
    private int clientPackageCount;


    public Menu() {
        menu.put(0, "   0 - Select all tables");
        menu.put(200, "   200 - Select structure of DB");
        menu.put(1, "   1 - Table: Client");
        menu.put(11, "  11 - Create for Client");
        menu.put(12, "  12 - Update Client");
        menu.put(13, "  13 - Delete from Client");
        menu.put(14, "  14 - Select Client");
        menu.put(15, "  15 - Find Client by ID");
        menu.put(16, "  16 - Get from Client by surname");
        menu.put(2, "   2 - Table: DeliveryPoint");
        menu.put(21, "  21 - Create for DeliveryPoint");
        menu.put(22, "  22 - Update DeliveryPoint");
        menu.put(23, "  23 - Delete from DeliveryPoint");
        menu.put(24, "  24 - Select DeliveryPoint");
        menu.put(25, "  25 - Find DeliveryPoint by ID");
        menu.put(26, "  26 - Get address of DeliveryPoint by city");
        menu.put(3, "   3 - Table: GettingPackage");
        menu.put(31, "  31 - Create for GettingPackage ");
        menu.put(32, "  32 - Update GettingPackage");
        menu.put(33, "  33 - Delete from GettingPackage");
        menu.put(34, "  34 - Select GettingPackage");
        menu.put(35, "  35 - Find GettingPackage by ID");
        menu.put(36, "  36 - Update the value whether the package is got");
        menu.put(37, "  37 - Delete value from getting package and move to " +
                "redirected package");
        menu.put(4, "   4 - Table: Package");
        menu.put(41, "  41 - Create for Package");
        menu.put(42, "  42 - Update Package");
        menu.put(43, "  43 - Delete from Package");
        menu.put(44, "  44 - Select Package");
        menu.put(45, "  45 - Find Package by ID");
        menu.put(5, "   5 - Table: RedirectedPackage");
        menu.put(51, "  51 - Create for RedirectedPackage");
        menu.put(52, "  52 - Update RedirectedPackage");
        menu.put(53, "  53 - Delete from RedirectedPackage");
        menu.put(54, "  54 - Select RedirectedPackage");
        menu.put(55, "  55 - Find RedirectedPackage by ID");
        menu.put(6, "   6 - Table: SendingPackage");
        menu.put(61, "  61 - Create for SendingPackage");
        menu.put(62, "  62 - Update SendingPackage");
        menu.put(63, "  63 - Delete from SendingPackage");
        menu.put(64, "  64 - Select SendingPackage");
        menu.put(65, "  65 - Find SendingPackage by ID");
        menu.put(100, "   100 - exit");
//        methodsForMenu.put(0, this::getAll);
//        methodsForMenu.put(200, this::getAllStructure);
//        methodsForMenu.put(37, this::deleteAndMoveToRedirected);
        methodsForMenu.put(11, this::createClient);
        methodsForMenu.put(12, this::updateClient);
        methodsForMenu.put(13, this::deleteClient);
        methodsForMenu.put(14, this::showAllClients);
        methodsForMenu.put(15, this::showClientById);
//        methodsForMenu.put("16", this::deleteFromDepartmentMoveEmployees);
//        methodsForMenu.put("21", this::createForEmployee);
//        methodsForMenu.put("22", this::updateEmployee);
//        methodsForMenu.put("23", this::deleteFromEmployee);
//        methodsForMenu.put("24", this::selectEmployee);
//        methodsForMenu.put("25", this::findEmployeeByID);
//        methodsForMenu.put("26", this::findEmployeeByName);
//        methodsForMenu.put("31", this::createForProject);
//        methodsForMenu.put("32", this::updateProject);
//        methodsForMenu.put("33", this::deleteFromProject);
//        methodsForMenu.put("34", this::selectProject);
//        methodsForMenu.put("35", this::findProjectByID);
//        methodsForMenu.put("41", this::createForWorksOn);
//        methodsForMenu.put("42", this::updateWorksOn);
//        methodsForMenu.put("43", this::deleteFromWorksOn);
//        methodsForMenu.put("44", this::selectWorksOn);
    }

    public void show() {
        out();
    }

    @Override
    protected void showInfo() {
        LOGGER.info("Menu for task19_Hibernate");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    private void showAllClients() {
        LOGGER.info(CONTROLLER.getAllClients());
    }

    private void showClientById() {
        LOGGER.info("Please, input id of our client");
        String client = CONTROLLER.getClientByID(scan.nextInt());
        if(client == null){
            badInput();
            return;
        } else {
            LOGGER.info(client);
        }
    }

    private void createClient() {
        inputClient();
        CONTROLLER.createClient(clientId, clientName, clientSurname,
                clientMiddleName, clientPackageCount);
    }

    private void updateClient() {
        inputClient();
        CONTROLLER.updateClient(clientId, clientName, clientSurname,
                clientMiddleName, clientPackageCount);
    }

    private void deleteClient() {
        LOGGER.info("Enter id of a future deleted client");
        CONTROLLER.deleteClient(scan.nextInt());
    }

    private void inputClient() {
        LOGGER.info("Please, input id");
        clientId = scan.nextInt();
        LOGGER.info("Please, input name");
        clientName = scan.next();
        LOGGER.info("Please, input surname");
        clientSurname = scan.next();
        LOGGER.info("Please, input middle name");
        clientMiddleName= scan.next();
        LOGGER.info("Please, input package count");
        clientPackageCount = scan.nextInt();
    }

    /*
                String logMessage = "Error with parsing with Jackson";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
     */
}

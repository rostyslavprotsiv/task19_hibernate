//package com.rostyslavprotsiv.model.service;
//
//import com.rostyslavprotsiv.model.DAO.implementation.DeliveryPointDAOImpl;
//import com.rostyslavprotsiv.model.entity.DeliveryPoint;
//
//import java.sql.SQLException;
//import java.util.List;
//
//public class DeliveryPointService {
//
//    public List<DeliveryPoint> findAll() throws SQLException {
//        return new DeliveryPointDAOImpl().findAll();
//    }
//
//    public DeliveryPoint findById(Integer id) throws SQLException {
//        return new DeliveryPointDAOImpl().findById(id);
//    }
//
//    public int create(DeliveryPoint entity) throws SQLException {
//        return new DeliveryPointDAOImpl().create(entity);
//    }
//
//    public int update(DeliveryPoint entity) throws SQLException {
//        return new DeliveryPointDAOImpl().update(entity);
//    }
//
//    public int delete(Integer id) throws SQLException {
//        return new DeliveryPointDAOImpl().delete(id);
//    }
//
//    public List<String> getAddressByCity(String city) throws SQLException {
//        return new DeliveryPointDAOImpl().getAddressByCity(city);
//    }
//}

//package com.rostyslavprotsiv.model.service;
//
//import com.rostyslavprotsiv.model.DAO.implementation.PackageDAOImpl;
//import com.rostyslavprotsiv.model.entity.Package;
//
//import java.sql.SQLException;
//import java.util.List;
//public class PackageService {
//
//    public List<Package> findAll() throws SQLException {
//        return new PackageDAOImpl().findAll();
//    }
//
//    public Package findById(Integer id) throws SQLException {
//        return new PackageDAOImpl().findById(id);
//    }
//
//    public int create(Package entity) throws SQLException {
//        return new PackageDAOImpl().create(entity);
//    }
//
//    public int update(Package entity) throws SQLException {
//        return new PackageDAOImpl().update(entity);
//    }
//
//    public int delete(Integer id) throws SQLException {
//        return new PackageDAOImpl().delete(id);
//    }
//}

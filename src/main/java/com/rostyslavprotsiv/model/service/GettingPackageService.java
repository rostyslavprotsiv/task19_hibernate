//package com.rostyslavprotsiv.model.service;
//
//import com.rostyslavprotsiv.model.DAO.implementation.GettingPackageDAOImpl;
//import com.rostyslavprotsiv.model.DAO.implementation.RedirectedPackageDAOImpl;
//import com.rostyslavprotsiv.model.connectionmanager.ConnectionManager;
//import com.rostyslavprotsiv.model.entity.GettingPackage;
//import com.rostyslavprotsiv.model.entity.RedirectedPackage;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.List;
//
//public class GettingPackageService {
//
//    public List<GettingPackage> findAll() throws SQLException {
//        return new GettingPackageDAOImpl().findAll();
//    }
//
//    public GettingPackage findById(Integer id) throws SQLException {
//        return new GettingPackageDAOImpl().findById(id);
//    }
//
//    public int create(GettingPackage entity) throws SQLException {
//        return new GettingPackageDAOImpl().create(entity);
//    }
//
//    public int update(GettingPackage entity) throws SQLException {
//        return new GettingPackageDAOImpl().update(entity);
//    }
//
//    public int delete(Integer id) throws SQLException {
//        return new GettingPackageDAOImpl().delete(id);
//    }
//
//    void updateIsGotById(boolean isGot, Integer id) throws SQLException {
//        new GettingPackageDAOImpl().updateIsGotById(isGot, id);
//    }
//
//    public boolean deleteAndMoveToRedirected(int deletedId) throws SQLException {
//        Connection connection = ConnectionManager.getConnection();
//        GettingPackageDAOImpl gettingPackageDAO = new GettingPackageDAOImpl();
//        RedirectedPackageDAOImpl redirectedPackageDAO =
//                new RedirectedPackageDAOImpl();
//        RedirectedPackage redirectedPackage = new RedirectedPackage();
//        GettingPackage gettingPackage;
//        try {
//            connection.setAutoCommit(false);
//            gettingPackage = gettingPackageDAO.findById(deletedId);
//            if(gettingPackage != null) {
//                redirectedPackage.setSendingPackageId(gettingPackage
//                        .getSendingPackageId());
//                redirectedPackage.setToClientId(gettingPackage.getToClientId());
//                redirectedPackage.setToPointId(gettingPackage.getToPointId());
//                redirectedPackageDAO.createWithoutID(redirectedPackage);
//                gettingPackageDAO.delete(deletedId);
//                connection.commit();
//            } else {
//                connection.rollback();
//                return false;
//            }
//        } catch (SQLException e) {
//            if (connection != null) {
//                System.err.print("Transaction is being rolled back \n");
//                e.printStackTrace();
//                connection.rollback();
//                return false;
//            }
//        } finally {
//            connection.setAutoCommit(true);
//        }
//        return true;
//    }
//}

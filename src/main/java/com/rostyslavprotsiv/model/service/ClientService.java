package com.rostyslavprotsiv.model.service;

import com.rostyslavprotsiv.model.DAO.implementation.ClientDAOImpl;
import com.rostyslavprotsiv.model.hibernate.ClientEntity;

import java.util.List;

public class ClientService {
    public List<ClientEntity> findAll() {
        return new ClientDAOImpl().findAll();
    }

    public ClientEntity findById(Integer id) {
        return new ClientDAOImpl().findById(id);
    }

    public void create(ClientEntity entity) {
        new ClientDAOImpl().create(entity);
    }

    public void update(ClientEntity entity) {
        new ClientDAOImpl().update(entity);
    }

    public void delete(Integer id) {
        new ClientDAOImpl().delete(id);
    }

    public List<ClientEntity> getBySurname(String surname) {
        return new ClientDAOImpl().getBySurname(surname);
    }
}

//package com.rostyslavprotsiv.model.service;
//
//import com.rostyslavprotsiv.model.DAO.implementation.SendingPackageDAOImpl;
//import com.rostyslavprotsiv.model.entity.SendingPackage;
//
//import java.sql.SQLException;
//import java.util.List;
//
//public class SendingPackageService {
//
//    public List<SendingPackage> findAll() throws SQLException {
//        return new SendingPackageDAOImpl().findAll();
//    }
//
//    public SendingPackage findById(Integer id) throws SQLException {
//        return new SendingPackageDAOImpl().findById(id);
//    }
//
//    public int create(SendingPackage entity) throws SQLException {
//        return new SendingPackageDAOImpl().create(entity);
//    }
//
//    public int update(SendingPackage entity) throws SQLException {
//        return new SendingPackageDAOImpl().update(entity);
//    }
//
//    public int delete(Integer id) throws SQLException {
//        return new SendingPackageDAOImpl().delete(id);
//    }
//
//}

package com.rostyslavprotsiv.model.hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "getting_package", schema = "new_post", catalog = "")
@IdClass(GettingPackageEntityPK.class)
public class GettingPackageEntity {
    private Integer id;
    private Byte got;
    private Integer price;
    private Integer sendingPackageId;
    private Integer toClientId;
    private Integer toPointId;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "got", nullable = true)
    public Byte getGot() {
        return got;
    }

    public void setGot(Byte got) {
        this.got = got;
    }

    @Basic
    @Column(name = "price", nullable = true, precision = 0)
    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Id
    @Column(name = "sending_package_id", nullable = false)
    public Integer getSendingPackageId() {
        return sendingPackageId;
    }

    public void setSendingPackageId(Integer sendingPackageId) {
        this.sendingPackageId = sendingPackageId;
    }

    @Id
    @Column(name = "to_client_id", nullable = false)
    public Integer getToClientId() {
        return toClientId;
    }

    public void setToClientId(Integer toClientId) {
        this.toClientId = toClientId;
    }

    @Id
    @Column(name = "to_point_id", nullable = false)
    public Integer getToPointId() {
        return toPointId;
    }

    public void setToPointId(Integer toPointId) {
        this.toPointId = toPointId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GettingPackageEntity that = (GettingPackageEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(got, that.got) &&
                Objects.equals(price, that.price) &&
                Objects.equals(sendingPackageId, that.sendingPackageId) &&
                Objects.equals(toClientId, that.toClientId) &&
                Objects.equals(toPointId, that.toPointId);
    }

    @Override
    public int hashCode() {
        return Objects
                .hash(id, got, price, sendingPackageId, toClientId, toPointId);
    }

    @Override
    public String toString() {
        return "GettingPackageEntity{" +
                "id=" + id +
                ", got=" + got +
                ", price=" + price +
                ", sendingPackageId=" + sendingPackageId +
                ", toClientId=" + toClientId +
                ", toPointId=" + toPointId +
                '}';
    }
}

package com.rostyslavprotsiv.model.hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "redirected_package", schema = "new_post", catalog = "")
@IdClass(RedirectedPackageEntityPK.class)
public class RedirectedPackageEntity {
    private Integer id;
    private Integer sendingPackageId;
    private Integer toClientId;
    private Integer toPointId;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Id
    @Column(name = "sending_package_id", nullable = false)
    public Integer getSendingPackageId() {
        return sendingPackageId;
    }

    public void setSendingPackageId(Integer sendingPackageId) {
        this.sendingPackageId = sendingPackageId;
    }

    @Id
    @Column(name = "to_client_id", nullable = false)
    public Integer getToClientId() {
        return toClientId;
    }

    public void setToClientId(Integer toClientId) {
        this.toClientId = toClientId;
    }

    @Id
    @Column(name = "to_point_id", nullable = false)
    public Integer getToPointId() {
        return toPointId;
    }

    public void setToPointId(Integer toPointId) {
        this.toPointId = toPointId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RedirectedPackageEntity that = (RedirectedPackageEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(sendingPackageId, that.sendingPackageId) &&
                Objects.equals(toClientId, that.toClientId) &&
                Objects.equals(toPointId, that.toPointId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sendingPackageId, toClientId, toPointId);
    }

    @Override
    public String toString() {
        return "RedirectedPackageEntity{" +
                "id=" + id +
                ", sendingPackageId=" + sendingPackageId +
                ", toClientId=" + toClientId +
                ", toPointId=" + toPointId +
                '}';
    }
}

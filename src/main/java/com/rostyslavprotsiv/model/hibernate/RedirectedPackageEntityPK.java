package com.rostyslavprotsiv.model.hibernate;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class RedirectedPackageEntityPK implements Serializable {
    private Integer id;
    private Integer sendingPackageId;
    private Integer toClientId;
    private Integer toPointId;

    @Column(name = "id", nullable = false)
    @Id
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "sending_package_id", nullable = false)
    @Id
    public Integer getSendingPackageId() {
        return sendingPackageId;
    }

    public void setSendingPackageId(Integer sendingPackageId) {
        this.sendingPackageId = sendingPackageId;
    }

    @Column(name = "to_client_id", nullable = false)
    @Id
    public Integer getToClientId() {
        return toClientId;
    }

    public void setToClientId(Integer toClientId) {
        this.toClientId = toClientId;
    }

    @Column(name = "to_point_id", nullable = false)
    @Id
    public Integer getToPointId() {
        return toPointId;
    }

    public void setToPointId(Integer toPointId) {
        this.toPointId = toPointId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RedirectedPackageEntityPK that = (RedirectedPackageEntityPK) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(sendingPackageId, that.sendingPackageId) &&
                Objects.equals(toClientId, that.toClientId) &&
                Objects.equals(toPointId, that.toPointId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sendingPackageId, toClientId, toPointId);
    }
}

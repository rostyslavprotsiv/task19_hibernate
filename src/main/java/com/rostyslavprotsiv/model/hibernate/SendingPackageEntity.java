package com.rostyslavprotsiv.model.hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "sending_package", schema = "new_post", catalog = "")
@IdClass(SendingPackageEntityPK.class)
public class SendingPackageEntity {
    private Integer id;
    private Integer sendingId;
    private Integer fromClientId;
    private Integer fromPointId;
    private Integer packageId;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "sending_id", nullable = false)
    public Integer getSendingId() {
        return sendingId;
    }

    public void setSendingId(Integer sendingId) {
        this.sendingId = sendingId;
    }

    @Id
    @Column(name = "from_client_id", nullable = false)
    public Integer getFromClientId() {
        return fromClientId;
    }

    public void setFromClientId(Integer fromClientId) {
        this.fromClientId = fromClientId;
    }

    @Id
    @Column(name = "from_point_id", nullable = false)
    public Integer getFromPointId() {
        return fromPointId;
    }

    public void setFromPointId(Integer fromPointId) {
        this.fromPointId = fromPointId;
    }

    @Id
    @Column(name = "package_id", nullable = false)
    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SendingPackageEntity that = (SendingPackageEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(sendingId, that.sendingId) &&
                Objects.equals(fromClientId, that.fromClientId) &&
                Objects.equals(fromPointId, that.fromPointId) &&
                Objects.equals(packageId, that.packageId);
    }

    @Override
    public int hashCode() {
        return Objects
                .hash(id, sendingId, fromClientId, fromPointId, packageId);
    }

    @Override
    public String toString() {
        return "SendingPackageEntity{" +
                "id=" + id +
                ", sendingId=" + sendingId +
                ", fromClientId=" + fromClientId +
                ", fromPointId=" + fromPointId +
                ", packageId=" + packageId +
                '}';
    }
}

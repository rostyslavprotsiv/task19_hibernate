package com.rostyslavprotsiv.model.hibernate;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class SendingPackageEntityPK implements Serializable {
    private Integer id;
    private Integer fromClientId;
    private Integer fromPointId;
    private Integer packageId;

    @Column(name = "id", nullable = false)
    @Id
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "from_client_id", nullable = false)
    @Id
    public Integer getFromClientId() {
        return fromClientId;
    }

    public void setFromClientId(Integer fromClientId) {
        this.fromClientId = fromClientId;
    }

    @Column(name = "from_point_id", nullable = false)
    @Id
    public Integer getFromPointId() {
        return fromPointId;
    }

    public void setFromPointId(Integer fromPointId) {
        this.fromPointId = fromPointId;
    }

    @Column(name = "package_id", nullable = false)
    @Id
    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SendingPackageEntityPK that = (SendingPackageEntityPK) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(fromClientId, that.fromClientId) &&
                Objects.equals(fromPointId, that.fromPointId) &&
                Objects.equals(packageId, that.packageId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fromClientId, fromPointId, packageId);
    }
}

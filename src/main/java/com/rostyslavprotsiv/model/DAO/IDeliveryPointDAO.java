package com.rostyslavprotsiv.model.DAO;


import com.rostyslavprotsiv.model.hibernate.DeliveryPointEntity;

import java.util.List;

public interface IDeliveryPointDAO extends IGeneralDAO<DeliveryPointEntity, Integer> {
    List<String> getAddressByCity(String city);
}

package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.hibernate.PackageEntity;

public interface IPackageDAO extends IGeneralDAO<PackageEntity, Integer> {
}

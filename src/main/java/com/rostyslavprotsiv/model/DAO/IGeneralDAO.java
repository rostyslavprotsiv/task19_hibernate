package com.rostyslavprotsiv.model.DAO;

import java.util.List;

public interface IGeneralDAO<T, ID> {
    List<T> findAll();

    T findById(ID id);

    void create(T entity);

    void update(T entity);

    void delete(ID id);
}

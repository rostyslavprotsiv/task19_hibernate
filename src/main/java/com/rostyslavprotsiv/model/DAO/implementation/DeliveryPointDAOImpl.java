package com.rostyslavprotsiv.model.DAO.implementation;

import com.rostyslavprotsiv.model.DAO.IDeliveryPointDAO;
import com.rostyslavprotsiv.model.DAO.entitymanagersession.UsingEntityManager;
import com.rostyslavprotsiv.model.hibernate.DeliveryPointEntity;

import javax.persistence.EntityManager;
import java.util.LinkedList;
import java.util.List;

public class DeliveryPointDAOImpl implements IDeliveryPointDAO {
    private static final String FIND_ALL = "SELECT * FROM delivery_point";
    private static final String DELETE = "DELETE FROM delivery_point WHERE " +
            "id=?";
    private static final String CREATE = "INSERT delivery_point (id, address," +
            "city, point_id) VALUES (?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE delivery_point SET address=?, " +
            "city=?, point_id=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM delivery_point" +
            " WHERE id=?";
    private static final String FIND_ADDRESS_BY_CITY = "SELECT address FROM " +
            "delivery_point WHERE city=?";

    @Override
    public List<DeliveryPointEntity> findAll() {
        List<DeliveryPointEntity> deliveryPoints = new LinkedList<>();
        EntityManager manager = UsingEntityManager.getEm();
        manager.find(DeliveryPointEntity.class, deliveryPoints);
        return deliveryPoints;
    }

    @Override
    public DeliveryPointEntity findById(Integer id) {
        EntityManager manager = UsingEntityManager.getEm();
        DeliveryPointEntity entity = manager.find(DeliveryPointEntity.class, id);
        return entity;
    }

    @Override
    public void create(DeliveryPointEntity entity) {
        EntityManager manager = UsingEntityManager.getEm();
        manager.getTransaction().begin();
        manager.persist(entity);
        manager.flush();
        manager.getTransaction().commit();
    }

    @Override
    public void update(DeliveryPointEntity entity){

    }

    @Override
    public void delete(Integer id) {
        EntityManager manager = UsingEntityManager.getEm();
        manager.getTransaction().begin();
        DeliveryPointEntity entity = manager.find(DeliveryPointEntity.class, id);
        manager.remove(entity);
        manager.getTransaction().commit();
    }

    @Override
    public List<String> getAddressByCity(String city) {
        return null;
    }

    public static void main(String[] args) {
        DeliveryPointDAOImpl deliveryPointDAO = new DeliveryPointDAOImpl();
        System.out.println(deliveryPointDAO.findById(1));
//        DeliveryPointEntity entity = new DeliveryPointEntity();
//        entity.setId(3);
//        entity.setAddress("asafffdf");
//        entity.setCity("Moskffo");
//        entity.setPointId("3bhffffj3vh");
//        deliveryPointDAO.create(entity);
//        entity.setId(4);
//        entity.setAddress("avfff");
//        entity.setCity("Mfffov");
//        entity.setPointId("tffff5bhj3vh");
//        deliveryPointDAO.create(entity);
//        System.out.println(deliveryPointDAO.findById(4));
//        deliveryPointDAO.delete(6);
//        System.out.println(deliveryPointDAO.findById(4));
        deliveryPointDAO.findAll().forEach(System.out::println);
    }

}

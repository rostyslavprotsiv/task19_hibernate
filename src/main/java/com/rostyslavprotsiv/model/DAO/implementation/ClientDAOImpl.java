package com.rostyslavprotsiv.model.DAO.implementation;

import com.rostyslavprotsiv.model.DAO.IClientDAO;
import com.rostyslavprotsiv.model.DAO.entitymanagersession.UsingSesssion;
import com.rostyslavprotsiv.model.hibernate.ClientEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class ClientDAOImpl implements IClientDAO {
    private static final String FIND_ALL = "FROM ClientEntity";
    private static final String UPDATE = "UPDATE ClientEntity SET name=:par1," +
            " surname=:par2, middleName=:par3,packageCount=:par4 WHERE " +
            "id=:par0";
    private static final String FIND_BY_SURNAME = "FROM ClientEntity" +
            " WHERE surname=:surn";

    @Override
    public List<ClientEntity> findAll() {
        return UsingSesssion.getSession().createQuery(FIND_ALL).list();
    }

    @Override
    public ClientEntity findById(Integer id) {
        return UsingSesssion.getSession().load(ClientEntity.class, id);
    }

    @Override
    public void create(ClientEntity entity) {
        Session session = UsingSesssion.getSession();
        session.beginTransaction();
        session.save(entity);
        session.getTransaction().commit();
    }

    @Override
    public void update(ClientEntity entity) {
        Session session = UsingSesssion.getSession();
        session.beginTransaction();
        Query query = session.createQuery(UPDATE);
        query.setParameter("par0", entity.getId());
        query.setParameter("par1", entity.getName());
        query.setParameter("par2", entity.getSurname());
        query.setParameter("par3", entity.getMiddleName());
        query.setParameter("par4", entity.getPackageCount());
        query.executeUpdate();
        session.getTransaction().commit();
    }

    @Override
    public void delete(Integer id) {
        Session session = UsingSesssion.getSession();
        ClientEntity clientEntity = session.load(ClientEntity.class, id);
        session.delete(clientEntity);
        session.flush();
    }

    @Override
    public List<ClientEntity> getBySurname(String surname) {
        Session session = UsingSesssion.getSession();
        session.beginTransaction();
        Query query = session.createQuery(FIND_BY_SURNAME);
        query.setParameter("surn", surname);
        session.getTransaction().commit();
        return query.list();
    }
}

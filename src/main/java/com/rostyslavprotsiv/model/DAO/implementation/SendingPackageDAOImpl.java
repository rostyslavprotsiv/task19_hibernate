//package com.rostyslavprotsiv.model.DAO.implementation;
//
//import com.rostyslavprotsiv.model.DAO.ISendingPackageDAO;
//import com.rostyslavprotsiv.model.hibernate.SendingPackageEntity;
//
//import java.sql.*;
//import java.util.LinkedList;
//import java.util.List;
//
//public class SendingPackageDAOImpl implements ISendingPackageDAO {
//    private static final String FIND_ALL = "SELECT * FROM sending_package";
//    private static final String DELETE = "DELETE FROM sending_package WHERE " +
//            "id=?";
//    private static final String CREATE = "INSERT sending_package (id, " +
//            "sending_id," +
//            "from_client_id, from_point_id, package_id) VALUES (?, ?, ?, ?, ?)";
//    private static final String UPDATE = "UPDATE sending_package SET " +
//            "sending_id=?, " +
//            "from_client_id=?, from_point_id=?, package_id=? WHERE id=?";
//    private static final String FIND_BY_ID = "SELECT * FROM sending_package " +
//            "WHERE id=?";
//
//
//    @Override
//    public List<SendingPackageEntity> findAll(){
//        List<SendingPackageEntity> sendingPackages = new LinkedList<>();
//        Connection connection = ConnectionManager.getConnection();
//        try (Statement statement = connection.createStatement()) {
//            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
//                while (resultSet.next()) {
//                    sendingPackages.add((SendingPackage)
//                            new Transformer(SendingPackage.class)
//                            .fromResultSetToEntity(resultSet));
//                }
//            }
//        }
//        return sendingPackages;
//    }
//
//    @Override
//    public SendingPackage findById(Integer id) throws SQLException {
//        SendingPackage entity = null;
//        Connection connection = ConnectionManager.getConnection();
//        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
//            ps.setInt(1, id);
//            try (ResultSet resultSet = ps.executeQuery()) {
//                while (resultSet.next()) {
//                    entity = (SendingPackage)
//                            new Transformer(SendingPackage.class)
//                            .fromResultSetToEntity(resultSet);
//                    break;
//                }
//            }
//        }
//        return entity;
//    }
//
//    @Override
//    public int create(SendingPackage entity) throws SQLException {
//        Connection conn = ConnectionManager.getConnection();
//        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
//            ps.setInt(1, entity.getId());
//            ps.setInt(2, entity.getSendingId());
//            ps.setInt(3, entity.getFromClientId());
//            ps.setInt(4, entity.getFromPointId());
//            ps.setInt(5, entity.getPackageId());
//            return ps.executeUpdate();
//        }
//    }
//
//    @Override
//    public int update(SendingPackage entity) throws SQLException {
//        Connection conn = ConnectionManager.getConnection();
//        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
//            ps.setInt(1, entity.getSendingId());
//            ps.setInt(2, entity.getFromClientId());
//            ps.setInt(3, entity.getFromPointId());
//            ps.setInt(4, entity.getPackageId());
//            ps.setInt(5, entity.getId());
//            return ps.executeUpdate();
//        }
//    }
//
//    @Override
//    public int delete(Integer id) throws SQLException {
//        Connection conn = ConnectionManager.getConnection();
//        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
//            ps.setInt(1, id);
//            return ps.executeUpdate();
//        }
//    }
//}

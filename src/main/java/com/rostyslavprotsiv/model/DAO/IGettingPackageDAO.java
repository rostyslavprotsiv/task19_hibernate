package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.hibernate.GettingPackageEntity;

import java.sql.SQLException;

public interface IGettingPackageDAO extends IGeneralDAO<GettingPackageEntity, Integer> {
    void updateIsGotById(boolean isGot, Integer id);
}

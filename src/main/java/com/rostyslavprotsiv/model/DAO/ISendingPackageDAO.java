package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.hibernate.SendingPackageEntity;

public interface ISendingPackageDAO extends IGeneralDAO<SendingPackageEntity, Integer> {
}

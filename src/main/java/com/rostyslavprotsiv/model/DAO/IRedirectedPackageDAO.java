package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.hibernate.RedirectedPackageEntity;

import java.sql.SQLException;

public interface IRedirectedPackageDAO extends IGeneralDAO<RedirectedPackageEntity, Integer> {
    public int createWithoutID(RedirectedPackageEntity entity);
}

package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.hibernate.ClientEntity;

import java.sql.SQLException;
import java.util.List;

public interface IClientDAO extends IGeneralDAO<ClientEntity, Integer> {
    List<ClientEntity> getBySurname(String surname) throws SQLException;
}

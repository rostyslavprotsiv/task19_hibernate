package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.hibernate.ClientEntity;
import com.rostyslavprotsiv.model.service.ClientService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.ObjectNotFoundException;

public final class Controller {
    private final Logger LOGGER = LogManager.getLogger(Controller.class);
    private final ClientService CLIENT_SERVICE = new ClientService();
//    private final DeliveryPointService DELIVERY_POINT_SERVICE
//            = new DeliveryPointService();
//    private final GettingPackageService GETTING_PACKAGE_SERVICE
//            = new GettingPackageService();
//    private final PackageService PACKAGE_SERVICE = new PackageService();
//    private final RedirectedPackageService REDIRECTED_PACKAGE_SERVICE
//            = new RedirectedPackageService();
//    private final SendingPackageService SENDING_PACKAGE_SERVICE
//            = new SendingPackageService();
//    private final MetaDataService META_DATA_SERVICE
//            = new MetaDataService();

    public String getAllClients() {
        StringBuilder all = new StringBuilder();
        CLIENT_SERVICE.findAll().forEach(s -> all.append(s).append("\n"));
        return all.toString();
    }

    public String getClientByID(Integer id) {
        try {
            return CLIENT_SERVICE.findById(id).toString();
        }catch (ObjectNotFoundException e) {
            return null;
        }
    }

    public void createClient(Integer id, String name, String surname,
                             String middleName, Integer packageCount) {
        CLIENT_SERVICE.create(generateClient(id, name, surname, middleName,
                packageCount));
    }

    public void updateClient(Integer id, String name, String surname,
                             String middleName, Integer packageCount) {
        CLIENT_SERVICE.update(generateClient(id, name, surname, middleName,
                packageCount));
    }


    public void deleteClient(Integer id) {
        CLIENT_SERVICE.delete(id);
    }

    public String getClientBySurname(String surname) {
        StringBuilder all = new StringBuilder();
        CLIENT_SERVICE.getBySurname(surname).forEach(s->all.append(s).append("\n"));
        return all.toString();
    }

    private ClientEntity generateClient(Integer id, String name, String surname,
                                        String middleName, Integer packageCount) {
        ClientEntity entity = new ClientEntity();
        entity.setId(id);
        entity.setName(name);
        entity.setSurname(surname);
        entity.setMiddleName(middleName);
        entity.setPackageCount(packageCount);
        return entity;
    }

    /*
                String logMessage = "Error with validating";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
     */
}
